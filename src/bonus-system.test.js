import { calculateBonuses } from './bonus-system';

describe('Bonus system', () => {
  it('Correctly calculates standard bonuses', () => {
    expect(calculateBonuses('Standard',      1)).toBeCloseTo(0.05,  2);
    expect(calculateBonuses('Standard',   9999)).toBeCloseTo(0.05,  2);
    expect(calculateBonuses('Standard',  10000)).toBeCloseTo(0.075, 2);
    expect(calculateBonuses('Standard',  50000)).toBeCloseTo(0.1,   2);
    expect(calculateBonuses('Standard', 100000)).toBeCloseTo(0.125, 2);
  });

  it('Correctly calculates premium bonuses', () => {
    expect(calculateBonuses('Premium',      1)).toBeCloseTo(0.1,  2);
    expect(calculateBonuses('Premium',   1000)).toBeCloseTo(0.1,  2);
    expect(calculateBonuses('Premium',  10000)).toBeCloseTo(0.15, 2);
    expect(calculateBonuses('Premium',  10001)).toBeCloseTo(0.15, 2);
    expect(calculateBonuses('Premium',  50000)).toBeCloseTo(0.2,  2);
    expect(calculateBonuses('Premium',  50001)).toBeCloseTo(0.2,  2);
    expect(calculateBonuses('Premium', 100001)).toBeCloseTo(0.25, 2);
  });

  it('Correctly calculates diamond bonuses', () => {
    expect(calculateBonuses('Diamond',  10001)).toBeCloseTo(0.3, 2);
    expect(calculateBonuses('Diamond',  50000)).toBeCloseTo(0.4, 2);
    expect(calculateBonuses('Diamond', 100000)).toBeCloseTo(0.5, 2);
    expect(calculateBonuses('Diamond', 100001)).toBeCloseTo(0.5, 2);
  });

  it('Handles negative amounts', () => {
    expect(calculateBonuses('Premium', -10)).toBeCloseTo(0.1, 2);
  });

  it('Ignores unrecognized programs', () => {
    expect(calculateBonuses('nonsense', 100000)).toBeCloseTo(0, 2);
  });
});
